# Moodle Plugins #

This README would normally document whatever steps are necessary to get your application up and running.

## Table of plugin moodle



| No   | Plugin Name                  | Function                                               |
| :--- | :--------------------------- | :----------------------------------------------------- |
| 1    | assign_enroll_course.zip     | For assign enrollment course                           |
| 2    | assign_unenroll_course.zip   | For unassign enrollment course                         |
| 3    | course_link_profiler.zip     | For store link profiler in course from profiler plugin |
| 4    | course_selector.zip          | -                                                      |
| 5    | coursetoprofiler.zip         | Profiler plugin for moodle                             |
| 6    | email.zip                    | -                                                      |
| 7    | email_reports.zip            | -                                                      |
| 8    | framework_selector.zip       | -                                                      |
| 9    | get_bbb_by_course_id.zip     | For get BBB data by course id                          |
| 10   | get_course.zip               | For get course from middleware/mobile                  |
| 11   | get_course_list_dropdown.zip | For get course list form middleware to set product     |
| 12   | get_course_url.zip           | For get course url when buy/purchase                   |
| 13   | get_course_url_by_email.zip  | For get course url by email                            |
| 14   | iomad.zip                    | -                                                      |
| 15   | iomad_dashboard.zip          | -                                                      |
| 16   | iomad_settings.zip           | -                                                      |
| 17   | iomad_signup.zip             | -                                                      |
| 18   | iomad_track.zip              | -                                                      |
| 19   | report_attendance.zip        | -                                                      |
| 20   | report_companies.zip         | -                                                      |
| 21   | report_completion.zip        | -                                                      |
| 22   | report_license.zip           | -                                                      |
| 23   | report_scorm_overview.zip    | -                                                      |
| 24   | report_users.zip             | -                                                      |
| 25   | set_user_limit_bbb.zip       | For Set user limit in bbb server                       |
| 26   | template_selector.zip        | -                                                      |
| 27   | update_enroll_user_time.zip  | For update enrollment user                             |
| 28   | userkey.zip                  | Plugin SSO in moodle (User key authentication)         |
|      |                              |                                                        |



### How do I get set up? ###

* Moodle plugins installation
  - Download the plugins that you want as zip file.
  - Install the plugin as usual via upload file.
  ![Scheme](install_plugin.png)
  - Follow the next step by step.
  - Done
* Opencart plugins installation 
